"""
Start het camerabeeld als het niet actief is, anders eens herstarten
"""

import psutil
import subprocess
import os
import datetime
import time
from pid import PidFile, PidFileAlreadyLockedError
# noinspection PyUnresolvedReferences
from systemd import journal

pname = "omxplayer.bin"
cameraip = "172.16.1.1"
networkerrorfile = "/tmp/camerasetup_networkerror"


def is_running():
    process = filter(lambda p: p.name() == pname, psutil.process_iter())

    for n in process:
        if pname in n.name():
            return True


def kill_it():
    process = filter(lambda p: p.name() == pname, psutil.process_iter())

    for n in process:
        if pname in n.name():
            n.kill()


def start_it():
    subprocess.Popen([pname, "-o", "local", "rtsp://..."])


def is_up(ip):
    """Is er een netwerkverbinding?"""
    response = os.system("ping -c 1 -w2 " + ip + " > /dev/null 2>&1")
    return response == 0


def there_was_a_networkerror():
    with open(networkerrorfile, "a"):
        pass


def was_there_a_networkerror():
    return os.path.isfile(networkerrorfile)


def networkerror_is_gone():
    os.unlink(networkerrorfile)


try:
    with PidFile(piddir="/tmp/"):
        while True:
            # Script blijft lopen totdat er terug een netwerkverbinding is, elke 5 min wordt netwerk gecheckt
            # om dan het camerabeeld op te starten
            if not is_up(cameraip):
                journal.send("De camera is niet bereikbaar")
                there_was_a_networkerror()
                time.sleep(300)
                continue

            # Netwerk is live...
            journal.send("De camera is bereikbaar")

            if not is_running():
                journal.send("Het camerabeeld was niet actief, wordt nu gestart")
                start_it()
                networkerror_is_gone()
                break
            # Het loopt wel maar om 8u mag het eens herstart worden
            elif is_running() and datetime.datetime.now().hour is 8:
                journal.send("Het camerabeeld was wel actief, maar wordt deze morgen eens herstart")
                kill_it()
                start_it()
                networkerror_is_gone()
                break
            elif was_there_a_networkerror():
                journal.send("De camera was hiervoor niet bereikbaar, we herstarten het camerabeeld")
                kill_it()
                start_it()
                networkerror_is_gone()
                break
            # Het loopt al en er is geen issue
            else:
                journal.send("Er is niets mis met het camerabeeld en de camera :)")
                break
except PidFileAlreadyLockedError:
    journal.send("Het script is nog actief van een vorige sessie!")
    pass
