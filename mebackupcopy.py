"""
Koppieert ManageEngine backups van G:\ (waar geen backups van wordt gemaakt) naar een map op P:\ (\\NAS10)
"""
import os
import re
import logging
import datetime
from filecmp import cmp
from shutil import copyfile

logging.basicConfig(filename='mebackupcopy.log', level=logging.INFO)
logging.info("Starten backup copy: {}".format(datetime.datetime.now()))

SRCDIR = "G:/ManageEngine"
BACKUPDIR = "P:/Beheer/0_ict/1_software/z_backups/ManageEngine"
dircontents = os.listdir(SRCDIR)
backupdircontents = os.listdir(BACKUPDIR)

currentbackups = []

# Eerst nieuwe backups naar doel koppiëren
for file in dircontents:
    if re.match(r'\d+-\w{3}-\d+-\d{4}-\d+-\d+\.zip', file, re.I):
        # Onthouden voor subiet
        currentbackups.append(file)
        if not os.path.isfile(BACKUPDIR + "/" + file) or not cmp(SRCDIR + "/" + file, BACKUPDIR + "/" + file):
            copyfile(SRCDIR + "/" + file, BACKUPDIR + "/" + file)
            logging.info("{} is gekoppieerd naar backupdir".format(file))
        else:
            logging.info("{} bestond al in backupdir en is exact hetzelfde".format(file))


# Dan een opkuis houden
if len(currentbackups) != 0:
    for file in backupdircontents:
        if file not in currentbackups:
            os.remove(BACKUPDIR + "/" + file)
            logging.info("{} is overbodig en is verwijderd uit backupdir".format(file))
else:
    logging.info("currentbackups list was leeg, geen bestanden verwijderd")

logging.info("Einde backup cop: {}".format(datetime.datetime.now()))
