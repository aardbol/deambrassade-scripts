if (Test-Path $ENV:UserProfile\W10AppsRemoved$((Get-Item "HKLM:SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue('ReleaseID')).ok) {
    Write-Host "Removing unwanted Store apps has already been done"
} else {
    Write-Host 'Delete unwanted Store apps...'
    Write-Host ' '

    Write-Host 'Removing Officehub...'
    Get-AppxPackage *officehub* | Remove-AppxPackage

    Write-Host 'Removing Skype app...'
    Get-AppxPackage *skypeapp* | Remove-AppxPackage

    Write-Host 'Removing Solitaire Collection...'
    Get-AppxPackage *solitairecollection* | Remove-AppxPackage

    Write-Host 'Removing Groove Music + Movies & TV...'
    Get-AppxPackage *zune* | Remove-AppxPackage

    write-host 'Removing Photos...'
    Get-appxpackage microsoft.windows.photos | Remove-AppxPackage

    write-host 'Removing Windows Feedback-hub...'
    Get-AppxPackage *WindowsFeedbackHub* | Remove-AppxPackage

    write-host 'Removing Mobile plans...'
    Get-appxpackage *Microsoft.OneConnect* | Remove-AppxPackage

    write-host 'Removing LinkedIn...'
    Get-appxpackage *linkedin* | Remove-AppxPackage

    write-host 'Removing Xbox apps...'
    Get-appxpackage *xbox* | where-object {$_.name -notlike "*xboxgamecallableui"} | Remove-AppxPackage

    write-host 'Removing People...'
    Get-appxpackage microsoft-people | Remove-AppxPackage
    
    Write-Host 'Removing 3D Builder...'
    Get-AppxPackage *3dbuilder* | Remove-AppxPackage

    write-host 'Removing Print3D...'
    Get-appxpackage *print3d* | Remove-AppxPackage

    write-host 'Removing Mixed Reality...'
    Get-appxpackage *mixedreality* | Remove-AppxPackage

    write-host 'Removing 3D Viewer...'
    Get-appxpackage *3dviewer* | Remove-AppxPackage

    Write-Host 'Done!'
    Write-Host 'Creating file in userprofile...'
    New-Item $ENV:UserProfile\W10AppsRemoved$((Get-Item "HKLM:SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue('ReleaseID')).ok -ItemType file
}